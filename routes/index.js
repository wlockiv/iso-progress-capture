var express = require('express');
var router = express.Router();

var Iso = require('../models/iso');

router.get('/', (req, res) => {
  Iso.aggregate([
    {
      '$project': {
        _id: false,
        isoTag: true,
        laborEst: true,
        laborLogged: {
          $sum: '$laborEntries.hoursWorked'
        },
        percentComplete: true
      }
    }
  ], (err, result) => {
    if (err) return console.log(err);
    console.log(result);
    res.render('index.ejs', { entries: result });
  })
});


module.exports = router;