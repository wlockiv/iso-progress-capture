var express = require('express');
var router = express.Router();

var Iso = require('../models/iso');


// router.get('/', (req, res) => {
//   let tag = req.query.tag;
//   Iso.findOne({ isoTag: tag }, (err, result) => {
//     if (err) return console.log(err);
//     res.render('iso.ejs', { iso: result });
//   });
// });

router.get('/', (req, res) => {
  Iso.getIsoByTag(req.query.tag,
    (err, result) => {
      console.log(result);
      res.render('iso.ejs', { iso: result });
    });
});

// router.get('/', (req, res) => {
//   let tag = req.query.tag;
//   Iso.aggregate([
//     {
//       '$match': {
//         isoTag: tag,
//       }
//     },
//     {
//       '$project': {
//         _id: false,
//         isoTag: true,
//         percentComplete: true,
//         laborEntries: true,
//       }
//     }
//   ], (err, result) => {
//     if (err) return console.log(err);
//     console.log(result);
//     res.render('iso.ejs', { iso: result });
//   });
// });

module.exports = router;
