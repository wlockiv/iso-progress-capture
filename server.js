var express = require('express');
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var mongoose = require('mongoose');
var database = require('./config/database');
var ejs = require('ejs');

// App Init
var app = express();

// Middleware
app.set('view-engine', ejs);
app.use(express.static('public'));

// Routes
var indexRoutes = require('./routes/index');
var isoRoutes = require('./routes/isos');
app.use('/', indexRoutes)
app.use('/iso', isoRoutes)

// Mongoose connection (db)
mongoose.connect(database.uri, { useNewUrlParser: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log('Connected to database.');
});

app.listen(3000, () => {
  console.log('Listening on 3000.');
});

module.exports