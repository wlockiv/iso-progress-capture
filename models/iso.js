var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var IsoSchema = mongoose.Schema({
  isoTag: {
    type: String,
  },
  laborEst: {
    type: Number,
  },
  laborEntries: [{
    date: Date,
    hoursWorked: Number
  }],
  percentComplete: [Schema.Types.Decimal128]
})

var Iso = module.exports = mongoose.model('Iso', IsoSchema, 'iso-progress');

module.exports.getIsoByTag = function(isoTag, callback) {
  var query = { isoTag: isoTag };
  Iso.findOne(query, callback);
}